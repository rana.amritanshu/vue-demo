<?php

use App\User;
use Carbon\Carbon;
use Faker\Generator as Faker;

$factory->define(User::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => bcrypt('123456'),
        'address' => $faker->address,
        'is_active' => random_int(0, 1),
        'created_at' => Carbon::now()->toDateTimeString()
    ];
});
