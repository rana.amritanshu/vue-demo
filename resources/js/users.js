import Vue from 'vue';
import UserList from './components/UserList.vue';

new Vue({
    el: "#root",
    components: {
        UserList
    }
});
