<?php

use Illuminate\Http\Request;
use App\User;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/users', function (Request $request) {
    $sort = 'id';
    $by = 'desc';
    $perPage = 10;

    if (isset($request->sort, $request->by)) {
        $sort = $request->sort;
        $by = $request->by;
    }
    if ($request->perPage) {
        $perPage = $request->perPage;
    }

    $user = new User;
    
    if ($request->search) {
        $user = $user->where('name', 'like', "%{$request->search}%");
    }
    $user = $user->orderBy($sort, $by)->paginate($perPage);
    
    return new App\Http\Resources\UserResource($user);
});
